var parseString = require('xml2js').parseString;
var xml = `<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
<s:Body>
    <startApplicationResponse xmlns="http://tempuri.org/">
        <startApplicationResult xmlns:a="http://schemas.datacontract.org/2004/07/License_Verification" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
            <a:License_Verification_Service.Result>
                <a:addressLine1>DR.RAMESH CHHEDA PC</a:addressLine1>
                <a:addressLine2>5154 Miller Rd  Ste I</a:addressLine2>
                <a:addressLine3>null</a:addressLine3>
                <a:applicationNo>20011605929</a:applicationNo>
                <a:city>Flint</a:city>
                <a:countryName>US</a:countryName>
                <a:licenseAuthority>null</a:licenseAuthority>
                <a:licenseNo>4301045264</a:licenseNo>
                <a:licenseSpecialty xmlns:b="http://schemas.microsoft.com/2003/10/Serialization/Arrays"/>
                <a:licenseStatus>null</a:licenseStatus>
                <a:licenseStatusCode>null</a:licenseStatusCode>
                <a:licenseType>Medical Doctor</a:licenseType>
                <a:licenseTypeCode>Medical Doctor</a:licenseTypeCode>
                <a:overallStatus>Active</a:overallStatus>
                <a:overallStatusCode>Active</a:overallStatusCode>
                <a:state>MI</a:state>
                <a:verificationDate>4/15/2020 7:21:09 PM</a:verificationDate>
                <a:verificationMethod>Web</a:verificationMethod>
                <a:zip>48507</a:zip>
            </a:License_Verification_Service.Result>
        </startApplicationResult>
    </startApplicationResponse>
</s:Body>
</s:Envelope>`
parseString(xml,{tagNameProcessors:[stripPrefix]} ,function (err, result) {
    var resp_json = 
    console.log(JSON.stringify(result, null, 2));
});