const express = require('express');
const app = express();
const soapRequest = require('easy-soap-request');
const converter = require('xml2js');
const parser = require('xml2json')
const processors = require('xml2js/lib/processors')

const port = 3000;
const bootstrap = { "boot": "success" };

var json_options = {
    object: true,
    reversible: true,
    coerce: true,
    sanitize: true,
    trim: true,
    arrayNotation: true,
    alternateTextNode: true
}

// bootstrap function

app.get('/', (req, res) => {
    res.send(JSON.stringify(bootstrap, null, 2))
})

const accela_url = 'https://accelaadapters.apps.lara.state.mi.us/LicenseVerificationService/License_Verification_Service.svc';
const accela_req_headers = {
    'Content-Type': 'text/xml;charset=UTF-8',
    'soapAction': 'startApplication',
};

const lara_url = 'https://www.lara.michigan.gov:443/L2KLicenseVerificationWS/L2KWSSoapHttpPort'
const lara_req_headers = {
    'Content-Type': 'text/xml',
    'soapAction': 'urn:L2KWSPortType/getLicenseVerificationInformation'
}

//code to expose the license verification 
app.get('/license_verification/accela/:license_num', (req, res) => {
    var xml_request = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:lic="http://schemas.datacontract.org/2004/07/License_Verification">
    <soapenv:Header/>
    <soapenv:Body>
       <tem:startApplication>
          <!--Optional:-->
          <tem:input>
             <lic:licenseNo>${req.params.license_num}</lic:licenseNo>
          </tem:input>
       </tem:startApplication>
    </soapenv:Body>
 </soapenv:Envelope>`;


    (async () => {
        try {
            const { response } = await soapRequest({ url: accela_url, headers: accela_req_headers, xml: xml_request, timeout: 10000 }); // Optional timeout parameter(milliseconds)
            const { headers, body, statusCode } = response;
          /*  converter.parseString(body, { tagNameProcessors: [processors.stripPrefix] }, (err, response) => {
                if (err)
                    res.status(500).send(JSON.stringify(err, null, 2));
                else {
                    var resp_json = response.Envelope.Body[0].startApplicationResponse[0].startApplicationResult[0]["License_Verification_Service.Result"][0];
                    var key = "licenseSpecialty";
                    delete resp_json[key];
                    res.status(200).send(JSON.stringify(resp_json, null, 2));
                }

            })*/
             var json = parser.toJson(response.body,json_options)
             res.status(200).send(JSON.stringify(json,null,4))


        }
        catch (err) {
            if (err.code == 'ECONNABORTED') {
                res.status(408).send({
                    "error_code": "408",
                    "error_message": "service timeout"
                })
            }
            if (err.toString().includes('404')) {
                res.status(404).json({
                    "error_code": "404",
                    "error_message": "service not found"
                })
            }
            else {
                res.status(500).json({
                    "error_code": "500",
                    "error_message": "generic error"
                })
                console.log(err)
            }
        }
    })()
})

app.get('/license_verification/lara/:license_num', (req, res) => {
    var xml_request = `<?xml version="1.0" encoding="UTF-8"?>
    <soapenv:Envelope
        xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
        xmlns:xsd="http://www.w3.org/2001/XMLSchema"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <soapenv:Header>
            <ns1:L2KHeader
                soapenv:actor="http://schemas.xmlsoap.org/soap/actor/next"
                soapenv:mustUnderstand="0" xmlns:ns1="http://L2KWebService.xsd">
                <applicationPassword>volunteer</applicationPassword>
                <applicationUser>volunteer</applicationUser>
                <callingUser>VOLUNTEER</callingUser>
                <process>VOLUNTEER</process>
                <processId>VOLUNTEER</processId>
            </ns1:L2KHeader>
        </soapenv:Header>
        <soapenv:Body>
            <ns2:getLicenseVerificationInformation
                soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
                xmlns:ns2="urn:L2KWebService">
                <L2KInput>
                    <licenseNo>${req.params.license_num}</licenseNo>
                </L2KInput>
            </ns2:getLicenseVerificationInformation>
        </soapenv:Body>
    </soapenv:Envelope>`;


    (async () => {
        try {
            const { response } = await soapRequest({ url: lara_url, headers: lara_req_headers, xml: xml_request, timeout: 10000 }); // Optional timeout parameter(milliseconds)
            const { headers, body, statusCode } = response;
            /*converter.parseString(body, { tagNameProcessors: [processors.stripPrefix] }, (err, response) => {
                if (err)
                    res.status(500).send(JSON.stringify(err, null, 2));
                else {
                    var resp_json = response.Envelope.Body[0].getLicenseVerificationInformationResponse[0].return[0].item[0];
                    //console.log(JSON.stringify(resp_json,null,2))
                    res.status(200).send(JSON.stringify({
                        resp_json
                    }, null, 2));
                }

            })*/
            var json = parser.toJson(response.body)
             res.status(200).send(json['S:Envelope']['S:Body']['w:getLicenseVerificationInformationResponse'].return.item.licenseNo)

        }
        catch (err) {
            if (err.code == 'ECONNABORTED') {
                res.status(408).send({
                    "error_code": "408",
                    "error_message": "service timeout"
                })
            }
            if (err.toString().includes('404')) {
                res.status(404).json({
                    "error_code": "404",
                    "error_message": "service not found"
                })
            }
            else {
                res.status(500).json({
                    "error_code": "500",
                    "error_message": "generic error"
                })
                console.log(err)
            }
        }
    })()
})

app.get('*', (req, res) => {
    res.status(404).send(JSON.stringify({
        error_code: '404',
        error_message: 'service not found'
    }, null, 2))
})

app.listen(port, () => console.log('App started on port: ' + port));