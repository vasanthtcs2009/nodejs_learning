var parser = require('xml2json');

var xml_request = `<?xml version='1.0' encoding='UTF-8'?>
<S:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <env:Header/>
    <S:Body>
        <w:getLicenseVerificationInformationResponse xmlns:w="urn:L2KWebService">
            <return>
                <item>
                    <applicationNo>1994278</applicationNo>
                    <countryCode>1</countryCode>
                    <countryName>United States</countryName>
                    <licenseAuthority>State of Michigan</licenseAuthority>
                    <licenseNo>4401005184</licenseNo>
                    <licenseStatus>1</licenseStatus>
                    <overallStatus>Active</overallStatus>
                    <overallStatusCode>Verified</overallStatusCode>
                    <verificationDate>05052020</verificationDate>
                    <verificationMethod>Realtime</verificationMethod>
                </item>
            </return>
        </w:getLicenseVerificationInformationResponse>
    </S:Body>
</S:Envelope>`

var options = {
    object: false,
    reversible: false,
    coerce: false,
    sanitize: true,
    trim: true,
    arrayNotation: false,
    alternateTextNode: false
}

var json = parser.toJson(xml_request,options)

console.log(json)