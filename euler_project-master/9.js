/*
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

a2 + b2 = c2
For example, 32 + 42 = 9 + 16 = 25 = 52.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
*/

function calculate_pythagorean() {
    for (let a = 1; a <= 999; a++) {
        for (let b = a, lb = 999 - a; b < lb; b++) {
            const c = 1000 - (a + b)
            if (Math.pow(a, 2) + Math.pow(b, 2) == Math.pow(c, 2)) {
                return console.log(a * b * c)
            }
        }
    }
}

calculate_pythagorean();