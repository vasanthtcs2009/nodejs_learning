/*
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
*/

function calculate_lcm(n) {
    var ans = 1;
    for (i = 1; i < n; i++) {
        ans *= i / calculate_gcd(i, ans)
    }
    return ans
}


function calculate_gcd(x, y) {
    if ((typeof x != 'number') || (typeof y != 'number'))
        return false
    else {
        x = Math.abs(x);
        y = Math.abs(y);
        while (y) {
            var t = y;
            y = x % y;
            x = t;
        }
        return x
    }
}

console.log(calculate_lcm(20));