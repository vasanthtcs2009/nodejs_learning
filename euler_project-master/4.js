function is_palindrome_string(num) {
    var source_string = num.toString().split("");
    var reverse_string = source_string.slice().reverse();
    if (source_string.join("") == reverse_string.join(""))
        return true
    else
        return false
}

function calculate_largest_palindrome(n) {
    var min_num = Math.pow(10, n - 1);
    var max_num = Math.pow(10, n) - 1;
    var maxPalindrome = 0, product;

    for (i = max_num; i >= min_num; i--) {
        for (j = i; j >= min_num; j--) {
            product = i * j;
            if (is_palindrome_string(product) && product > maxPalindrome)
                maxPalindrome = product
        }
    }
    return maxPalindrome
}

console.log(calculate_largest_palindrome(3))