/*
The sum of the squares of the first ten natural numbers is,

12+22+...+102=385
The square of the sum of the first ten natural numbers is,

(1+2+...+10)2=552=3025
Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025−385=2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
*/

var square_sum = 0, sum_square = 0;

function sum_of_squares(n) {
    for (i = n; i >= 1; i--) {
        sum_square += i * i;
    }
    return sum_square
}

function square_of_sum(n) {
    for (i = 1; i <= n; i++) {
        square_sum += i;
    }
    return square_sum * square_sum
}

function main(n) {
    return square_of_sum(n) - sum_of_squares(n);
}

console.log(main(100))