/*
The prime factors of 13195 are 5, 7, 13 and 29.
What is the largest prime factor of the number 600851475143 ?
*/

var num = 600851475143 , result_arr = [];
for (i = 2; i <= num; i++) {
    while (num % i == 0) {
        result_arr.push(i)
        num = num / i;
    }
}

console.log(result_arr[result_arr.length - 1]);

// To list all the prime factors 
/*
for (var j = 0; j < result_arr.length; j++) {
    console.log(result_arr[j])
}
*/