/*
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.
*/

var result = [];

function get_prime_list(number) {
    for (i = 2; i <= number; i++) {
        if (isPrime(i))
            result.push(i)
    }
    return result;
}

function isPrime(num) {
    if (num <= 1) {
        return true
    } else if (num <= 3) {
        return true
    } else if (num % 2 === 0 || num % 3 === 0) {
        return false
    }
    let i = 5
    while (i * i <= num) {
        if (num % i === 0 || num % (i + 2) === 0) {
            return false
        }
        i += 6
    }
    return true
}

console.log(get_prime_list(2000000).reduce((a, b) => a + b, 0));