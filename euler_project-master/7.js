/*
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?
*/
var result_array = [], num = 1,i=0;

function isPrime(num) {
    if (num <= 1) {
        return true
    } else if (num <= 3) {
        return true
    } else if (num % 2 === 0 || num % 3 === 0) {
        return false
    }
    let i = 5
    while (i * i <= num) {
        if (num % i === 0 || num % (i + 2) === 0) {
            return false
        }
        i += 6
    }
    return true
}

function calculate_prime(n){
    var last_index = n-1;
    while(result_array.length <=last_index)
    {
        if(isPrime(num))
        {
            result_array.push(num)
        }
        num++;
    }
    return result_array[last_index];
    
}

console.log(calculate_prime(10001))